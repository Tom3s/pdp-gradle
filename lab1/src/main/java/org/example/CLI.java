package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CLI {
    //    public String[] getCommandWithArgs() throws IOException {
//        // read from stdin
//        System.out.print(">>> ");
//        String commandLine = System.in.readLine();
//
//        return commandLine.split(" ");
//    }
    public String[] getCommandWithArgs() throws IOException {
        // Create a BufferedReader to read from stdin
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print(">>> ");
        String commandLine = reader.readLine();

        if (commandLine != null) {
            // Split the input into an array of strings
            return commandLine.split(" ");
        } else {
            // Handle the case where there's no input
            return new String[0]; // or you can return null if preferred
        }
    }

    public void printResult(double result) {
        System.out.println("Result: " + result);
    }

    public void printError(String command, int from, int to) {
        System.out.println("Command " + command + " takes " + from + " to " + to + " arguments");
    }

    public void printInvalidCommand(String command) {
        System.out.println("Invalid command: " + command);
    }
}
