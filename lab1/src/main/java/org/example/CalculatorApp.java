package org.example;

import java.io.IOException;
import java.util.Arrays;

public class CalculatorApp {
    public static final String COMMAND_EXIT = "exit";
    public static final String COMMAND_CLEAR = "clear";
    public static final String COMMAND_ADD = "+";
    public static final String COMMAND_SUBTRACT = "-";
    public static final String COMMAND_MULTIPLY = "*";
    public static final String COMMAND_DIVIDE = "/";
    public static final String COMMAND_MAX = "max";
    public static final String COMMAND_MIN = "min";
    public static final String COMMAND_SQRT = "sqrt";

    private Calculator calculator;
    private CLI cli;
    private String command;

    public CalculatorApp(Calculator calculator, CLI cli) {
        this.calculator = calculator;
        this.cli = cli;
        command = "";
    }

    public void start(){
        while (!command.equals(COMMAND_EXIT)) {
            try {
                var commandWithArgs = cli.getCommandWithArgs();
                command = commandWithArgs[0];
                String[] arguments = Arrays.copyOfRange(commandWithArgs, 1, commandWithArgs.length);
                var result = calculator.lastResult;
                switch (command) {
                    case COMMAND_ADD:
                        if (arguments.length == 1) {
                            result = calculator.add(Double.parseDouble(arguments[0]));
                        } else if (arguments.length == 2) {
                            result = calculator.add(Double.parseDouble(arguments[0]), Double.parseDouble(arguments[1]));
                        } else {
                            cli.printError(command, 1, 2);
                        }
                        break;
                    case COMMAND_SUBTRACT:
                        if (arguments.length == 1) {
                            result = calculator.subtract(Double.parseDouble(arguments[0]));
                        } else if (arguments.length == 2) {
                            result = calculator.subtract(Double.parseDouble(arguments[0]), Double.parseDouble(arguments[1]));
                        } else {
                            cli.printError(command, 1, 2);
                        }
                        break;
                    case COMMAND_MULTIPLY:
                        if (arguments.length == 1) {
                            result = calculator.multiply(Double.parseDouble(arguments[0]));
                        } else if (arguments.length == 2) {
                            result = calculator.multiply(Double.parseDouble(arguments[0]), Double.parseDouble(arguments[1]));
                        } else {
                            cli.printError(command, 1, 2);
                        }
                        break;
                    case COMMAND_DIVIDE:
                        if (arguments.length == 1) {
                            result = calculator.divide(Double.parseDouble(arguments[0]));
                        } else if (arguments.length == 2) {
                            result = calculator.divide(Double.parseDouble(arguments[0]), Double.parseDouble(arguments[1]));
                        } else {
                            cli.printError(command, 1, 2);
                        }
                        break;
                    case COMMAND_MAX:
                        if (arguments.length >= 1) {
                            var numbers = new double[arguments.length];
                            for (int i = 0; i < arguments.length; i++) {
                                numbers[i] = Double.parseDouble(arguments[i]);
                            }
                            result = calculator.max(numbers);
                        } else {
                            cli.printError(command, 1, Integer.MAX_VALUE);
                        }
                        break;
                    case COMMAND_MIN:
                        if (arguments.length >= 1) {
                            var numbers = new double[arguments.length];
                            for (int i = 0; i < arguments.length; i++) {
                                numbers[i] = Double.parseDouble(arguments[i]);
                            }
                            result = calculator.min(numbers);
                        } else {
                            cli.printError(command, 1, Integer.MAX_VALUE);
                        }
                        break;
                    case COMMAND_SQRT:
                        if (arguments.length == 1) {
                            result = calculator.sqrt(Double.parseDouble(arguments[0]));
                        } else {
                            cli.printError(command, 1, 1);
                        }
                        break;
                    case COMMAND_CLEAR:
                        result = calculator.clear();
                        break;
                    case COMMAND_EXIT:
                        break;
                    default:
                        cli.printInvalidCommand(command);
                        break;
                }
                cli.printResult(result);
            } catch (IOException e) {
                // TODO: handle this exception
                throw new RuntimeException(e);
            }
        }
    }

}
