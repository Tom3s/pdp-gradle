package org.example;

import java.util.Arrays;

public class Calculator {
    public double lastResult = 0;
    public double add(double a, double b) {
        lastResult = a + b;
        return lastResult;
    }

    public double add(double a) {
        lastResult += a;
        return lastResult;
    }

    public double subtract(double a, double b) {
        lastResult = a - b;
        return lastResult;
    }

    public double subtract(double a) {
        lastResult -= a;
        return lastResult;
    }

    public double multiply(double a, double b) {
        lastResult = a * b;
        return lastResult;
    }

    public double multiply(double a) {
        lastResult *= a;
        return lastResult;
    }

    public double divide(double a, double b) {
        lastResult = a / b;
        return lastResult;
    }

    public double divide(double a) {
        lastResult /= a;
        return lastResult;
    }

    public double clear() {
        lastResult = 0;
        return lastResult;
    }

    public double min(double ...numbers) {
        return  Arrays.stream(numbers).min().getAsDouble();
    }

    public double max(double ...numbers) {
        return  Arrays.stream(numbers).max().getAsDouble();
    }

    public double sqrt(double a) {
        lastResult = Math.sqrt(a);
        return lastResult;
    }
}
