package org.example;

public class Main {


    public static void main(String[] args) {
        var calculator = new Calculator();
        var cli = new CLI();
        var app = new CalculatorApp(calculator, cli);
        app.start();
    }
}