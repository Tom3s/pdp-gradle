package org.example;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator calculator;
    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
        calculator.clear();
    }

    @org.junit.jupiter.api.Test
    void add() {

        assertEquals(3, calculator.add(1, 2));
        assertEquals(5, calculator.add(2));
    }

    @org.junit.jupiter.api.Test
    void subtract() {
        assertEquals(3, calculator.subtract(5, 2));
        assertEquals(1, calculator.subtract(2));
    }


    @org.junit.jupiter.api.Test
    void multiply() {
        assertEquals(6, calculator.multiply(2, 3));
        assertEquals(12, calculator.multiply(2));
    }



    @org.junit.jupiter.api.Test
    void divide() {
        assertEquals(2, calculator.divide(6, 3));
        assertEquals(1, calculator.divide(2));
    }


    @org.junit.jupiter.api.Test
    void clear() {
        assertEquals(0, calculator.clear());
    }

    @org.junit.jupiter.api.Test
    void min() {
        assertEquals(1, calculator.min(1, 2, 3));
        assertEquals(1, calculator.min(1));
    }

    @org.junit.jupiter.api.Test
    void max() {
        assertEquals(3, calculator.max(1, 2, 3));
        assertEquals(1, calculator.max(1));
    }

    @org.junit.jupiter.api.Test
    void sqrt() {
        assertEquals(2, calculator.sqrt(4));
    }
}