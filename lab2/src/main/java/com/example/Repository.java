package com.example;


public interface Repository<T> {
    public void add(T item);
    public boolean contains(T item);
    public void remove(T item);
}
