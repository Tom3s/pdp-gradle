package com.example;

public class InMemoryRepo<T> {
    private final Repository<T> repo;

    public InMemoryRepo(Repository<T> repo) {
        this.repo = repo;
    }

    public void add(T item) {
        repo.add(item);
    }

    public boolean contains(T item) {
        return repo.contains(item);
    }

    public void remove(T item) {
        repo.remove(item);
    }
}
