package com.example;

import java.util.ArrayList;

public class ArrayListRepo<T> implements  Repository<T> {
    private final ArrayList<T> items = new ArrayList<T>();

    public void add(T item) {
        items.add(item);
    }

    public boolean contains(T item) {
        return items.contains(item);
    }

    public void remove(T item) {
        items.remove(item);
    }
}
