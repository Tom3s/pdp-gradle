package com.example;

public class TreeSetRepo<T> implements Repository<T>{
    private final java.util.TreeSet<T> items = new java.util.TreeSet<T>();

    public void add(T item) {
        items.add(item);
    }

    public boolean contains(T item) {
        return items.contains(item);
    }

    public void remove(T item) {
        items.remove(item);
    }
}
