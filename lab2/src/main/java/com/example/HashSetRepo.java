package com.example;

import java.util.HashSet;

public class HashSetRepo<T> implements Repository<T>{
    private final HashSet<T> items = new HashSet<T>();

    public void add(T item) {
        items.add(item);
    }

    public boolean contains(T item) {
        return items.contains(item);
    }

    public void remove(T item) {
        items.remove(item);
    }
}
