package com.example.benchmarks;

import com.example.ArrayListRepo;
import com.example.HashSetRepo;
import com.example.TreeSetRepo;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class Maps {
    @Param({"1", "31", "65", "101", "103", "1024", "10240", "65535", "214748"})
    public int size;

    private ArrayListRepo<Integer> arrayListRepo;
    private HashSetRepo<Integer> hashSetRepo;
    private TreeSetRepo<Integer> treeSetRepo;

    @Benchmark
    public void arrayListAdd(Blackhole consumer) {
        arrayListRepo = new ArrayListRepo<Integer>();
        IntStream.rangeClosed(0, size).forEach(i -> {
            arrayListRepo.add(i);
            consumer.consume(arrayListRepo);
        });
    }

    @Benchmark
    public void arrayListContains(Blackhole consumer) {
        initializeArrayListRepo();
        consumer.consume(IntStream.rangeClosed(0, size)
                .allMatch(arrayListRepo::contains));
    }

    @Benchmark
    public void arrayListRemove(Blackhole consumer) {
        initializeArrayListRepo();
        IntStream.rangeClosed(0, size).forEach(i -> {
            arrayListRepo.remove(i);
            consumer.consume(arrayListRepo);
        });
    }

    @Benchmark
    public void hasSetAdd(Blackhole consumer) {
        hashSetRepo = new HashSetRepo<>();
        IntStream.rangeClosed(0, size).forEach(i -> {
            hashSetRepo.add(i);
            consumer.consume(hashSetRepo);
        });
    }

    @Benchmark
    public void hasSetContains(Blackhole consumer) {
        initializeHashSetRepo();
        consumer.consume(IntStream.rangeClosed(0, size)
                .allMatch(hashSetRepo::contains));
    }

    @Benchmark
    public void hasSetRemove(Blackhole consumer) {
        initializeHashSetRepo();
        IntStream.rangeClosed(0, size).forEach(i -> {
            hashSetRepo.remove(i);
            consumer.consume(hashSetRepo);
        });
    }

    @Benchmark
    public void treeSetAdd(Blackhole consumer) {
        treeSetRepo = new TreeSetRepo<>();
        IntStream.rangeClosed(0, size).forEach(i -> {
            treeSetRepo.add(i);
            consumer.consume(treeSetRepo);
        });
    }

    @Benchmark
    public void treeSetContains(Blackhole consumer) {
        initializeTreeSetRepo();
        consumer.consume(IntStream.rangeClosed(0, size)
                .allMatch(treeSetRepo::contains));
    }

    @Benchmark
    public void treeSetRemove(Blackhole consumer) {
        initializeTreeSetRepo();
        IntStream.rangeClosed(0, size).forEach(i -> {
            treeSetRepo.remove(i);
            consumer.consume(treeSetRepo);
        });
    }

    private void initializeArrayListRepo() {
        if (arrayListRepo == null) {
            arrayListRepo = new ArrayListRepo<>();
            IntStream.rangeClosed(0, size)
                    .forEach(arrayListRepo::add);
        }
    }

    private void initializeHashSetRepo() {
        if (hashSetRepo == null) {
            hashSetRepo = new HashSetRepo<>();
            IntStream.rangeClosed(0, size)
                    .forEach(hashSetRepo::add);
        }
    }

    private void initializeTreeSetRepo() {
        if (treeSetRepo == null) {
            treeSetRepo = new TreeSetRepo<>();
            IntStream.rangeClosed(0, size)
                    .forEach(treeSetRepo::add);
        }
    }
}
