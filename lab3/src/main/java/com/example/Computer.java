package com.example;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;

public class Computer {
    public static BigInteger sum(BigInteger[] numbers) {
        return Arrays.stream(numbers).reduce(BigInteger.ZERO, BigInteger::add);
    }

    public static BigInteger average(BigInteger[] numbers) {
        return sum(numbers).divide(BigInteger.valueOf(numbers.length));
    }

    public static BigInteger[] top10Percent(BigInteger[] numbers) {
        return Arrays.stream(numbers)
                .sorted()
                .skip(numbers.length - numbers.length / 10)
                .sorted(Collections.reverseOrder())
                .toArray(BigInteger[]::new);
    }
}
