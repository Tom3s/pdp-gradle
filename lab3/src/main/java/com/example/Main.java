package com.example;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        var numbers = Generator.generateNumbers(256);
        System.out.println("Sum: " + Computer.sum(numbers));
        System.out.println("Average: " + Computer.average(numbers));
        System.out.println("Top 10%: " + Arrays.toString(Computer.top10Percent(numbers)));
    }


}