package com.example;

import java.math.BigInteger;
import java.util.Random;

public class Generator {
    public static final int NR_NUMBERS = 1024;

    public static BigInteger[] generateNumbers() {
        return generateNumbers(0);
    }
    public static BigInteger[] generateNumbers(int seed) {
        BigInteger[] numbers = new BigInteger[NR_NUMBERS];
        Random random = new Random(seed);
        for (int i = 0; i < NR_NUMBERS; i++) {
            numbers[i] = BigInteger.probablePrime(1024, random);
        }
        return numbers;
    }
}
