package com.example;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class ComputerTest {

    private BigInteger[] numbers;
    @BeforeEach
    void setUp() {
        numbers = Generator.generateNumbers();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void sum() {
        BigInteger sum = BigInteger.valueOf(0);
        for (BigInteger number : numbers) {
            sum = sum.add(number);
        }
        assertEquals(sum, Computer.sum(numbers));
    }

    @Test
    void average() {
        BigInteger sum = BigInteger.valueOf(0);
        for (BigInteger number : numbers) {
            sum = sum.add(number);
        }
        assertEquals(sum.divide(BigInteger.valueOf(numbers.length)), Computer.average(numbers));
    }

    @Test
    void top10Percent() {
        BigInteger[] sorted = numbers.clone();
        Arrays.sort(sorted, Collections.reverseOrder());
        BigInteger[] top10 = Arrays.copyOfRange(sorted, 0, sorted.length / 10);
        assertArrayEquals(top10, Computer.top10Percent(numbers));
    }
}