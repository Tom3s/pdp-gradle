package com.example;

import java.util.HashMap;
import java.util.Map;

public class ConnectionSolver {
    private Map<String, Integer> map = new HashMap<>(
            Map.of(
                    "kutya", 12345,
                    "cica", 12346,
                    "kacsa", 12347
            )
    );

    public int getPort(String host) {
        return map.get(host);
    }

    public String[] getNames() {
        return map.keySet().toArray(new String[0]);
    }

}
