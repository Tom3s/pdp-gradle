package com.example;

import java.util.ArrayList;
import java.util.function.Function;

public class Signal {
    private ArrayList<Function> listeners = new ArrayList<>();

    public void connect(Function listener) {
        listeners.add(listener);
    }

    public void emit(Object... args) {
        for (Function listener : listeners) {
            listener.apply(args);
        }
    }
}
