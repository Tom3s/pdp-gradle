package com.example;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class ChatApp {
	private List<ChatClient> clients = new ArrayList<>();
	private ConnectionSolver connectionSolver = new ConnectionSolver();

	private String clientName = "kutya";

	public ChatApp() {
		var names = connectionSolver.getNames();

		System.out.println("Select user:");

		for (int i = 0; i < names.length; i++) {
			System.out.println((i + 1) + ". " + names[i]);
		}

		var userInput = System.console().readLine();
		var selectedIndex = Integer.parseInt(userInput) - 1;
		while (selectedIndex < 0 || selectedIndex >= names.length) {
			System.out.println("Invalid selection");
			userInput = System.console().readLine();
			selectedIndex = Integer.parseInt(userInput) - 1;
		}

		var selectedName = names[selectedIndex];
		clientName = selectedName;
		var selectedPort = connectionSolver.getPort(selectedName);

		new Thread(() -> {
			var server = new ChatServer(selectedPort);
		}).start();
		// var client = new ChatClient(selectedPort, selectedName);
		clients.add(new ChatClient(selectedPort, selectedName, selectedName));
	}

	public void start() {
		Scanner scanner = new Scanner(System.in);
		while (true) {
			String message = scanner.nextLine();
			if (message.startsWith("!hello")) {
				var parts = message.split(" ");
				var name = parts[1];
				var port = connectionSolver.getPort(name);
				var newClient = new ChatClient(port, clientName, name);
				clients.add(newClient);
			} else if (message.startsWith("!byebye")) {
				// close all connections
				for (var client : clients) {
					client.close();
				}
				scanner.close();
			} else if (message.startsWith("!bye ")) {
				var parts = message.split(" ");
				var name = parts[1];
				// close connection to port
				Iterator<ChatClient> iterator = clients.iterator();
				while (iterator.hasNext()) {
					ChatClient client = iterator.next();
					if (client.host.equals(name)) {
						client.close();
						iterator.remove();
					}
				}
			} else {
				for (var client : clients) {
					client.sendMessage(message);
				}
			}
		}
	}
}
