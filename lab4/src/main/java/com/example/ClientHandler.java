package com.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientHandler implements Runnable {
    private Socket clientSocket;
    private ChatServer server;
    private BufferedReader clientIn;
    private PrintWriter clientOut;
    private String clientName;

    public ClientHandler(ChatServer server, Socket clientSocket) {
        this.server = server;
        this.clientSocket = clientSocket;
        try {
            clientIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            clientOut = new PrintWriter(clientSocket.getOutputStream(), true);
        } catch (IOException e) {
            // e.printStackTrace();
			System.out.println("ClientHandler exception: " + e.getMessage());
        }
    }

    public String getClientName() {
        return clientName;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public void sendMessage(String message) {
        clientOut.println(message);
    }

    @Override
    public void run() {
        try {
            // Get the client's name
            clientName = clientIn.readLine();
			if (clientName.startsWith("!hello ")) {
				clientName = clientName.substring(7);
				clientOut.println("!ack");
			} else {
				System.out.println("Invalid connection");
				clientSocket.close();
				return;
			}
            // System.out.println(clientName + " has joined the chat.");

            // Read messages from the client and broadcast to others
            String clientMessage;
            while ((clientMessage = clientIn.readLine()) != null) {
                server.broadcastMessage(clientMessage, this);
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            // Remove the client when they disconnect
            server.removeClient(this);
        }
    }
}
