package com.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
		//  java -cp build/classes/java/main com.example.Main
        var chatApp = new ChatApp();
		chatApp.start();
    }
}