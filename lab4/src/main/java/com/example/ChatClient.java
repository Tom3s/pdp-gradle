package com.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ChatClient {
    private static final String SERVER_IP = "localhost";
    private static int SERVER_PORT = 12345;
    public String name = "kutya";
    public String host = "kutya";

    private Socket socket;

    public ChatClient(int port, String name, String host) {
        SERVER_PORT = port;
        this.name = name;
        this.host = host;
        startClient();
    }

    private PrintWriter serverOut;

    public void startClient() {
        try {
            socket = new Socket(SERVER_IP, SERVER_PORT);

            BufferedReader serverIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            serverOut = new PrintWriter(socket.getOutputStream(), true);

            // Send the name to the server
            // serverOut.println(name);
			serverOut.println("!hello " + name);
			var responseMessage = serverIn.readLine();
			if (!responseMessage.startsWith("!ack")) {
				System.out.println("Server did not acknowledge connection, received: " + responseMessage);
				socket.close();
				return;
			}

            // Start a thread to handle server messages
            new Thread(() -> {
                String serverResponse;
                try {
                    while ((serverResponse = serverIn.readLine()) != null) {
                        System.out.println(serverResponse);
                    }
                } catch (IOException e) {
                    // e.printStackTrace();
					System.out.println("Server closed connection");
                }
            }).start();

        } catch (IOException e) {
            // e.printStackTrace();
			System.out.println("Server not available");
        }
    }

    public void sendMessage(String message) {
        serverOut.println(message);
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
//            throw new RuntimeException(e);
        }
    }
}
